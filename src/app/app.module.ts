import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import {routingInfo} from './app.route';
 import { ChatModule } from './chat/chat.module';
import { HttpClientModule } from '@angular/common/http';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import {GithubAuthInterceptor} from './interceptor.component'

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { IframeComponent } from './iframe/iframe.component';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    IframeComponent
  ],
  imports: [TypeaheadModule.forRoot(),
    BrowserModule,
    routingInfo,
    ChatModule,
    FormsModule,
    HttpClientModule
  ],
   providers: [
     {
    provide: HTTP_INTERCEPTORS,
    useClass: GithubAuthInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
