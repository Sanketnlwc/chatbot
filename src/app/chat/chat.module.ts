import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChatService } from './chat-service/chat.service';
import { ChatDialogComponent } from './chat-dialog/chat-dialog.component';
import { ProductLineDetailsComponent } from './chat-dialog/product-details.component';
import { OrderDetailsComponent } from './chat-dialog/order-details.component';
import { RMADetailsComponent } from './chat-dialog/rma-details.component';
import { FormsModule } from '@angular/forms';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { SlickModule } from 'ngx-slick';

@NgModule({
  imports: [
    TypeaheadModule.forRoot(),
    SlickModule.forRoot(),
    CommonModule,
    FormsModule
  ],
  declarations: [ChatDialogComponent,ProductLineDetailsComponent,OrderDetailsComponent,RMADetailsComponent],
  exports: [ ChatDialogComponent ],
  providers: [ChatService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
]
})
export class ChatModule { }
