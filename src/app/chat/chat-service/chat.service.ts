import { Injectable } from '@angular/core';
import { environment } from '../../../environments/environment';
import { ApiAiClient } from 'api-ai-javascript';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

//Message class for displaying messages in the component

export class Message {
  constructor(public content:string,public sentBy:string, public parameter:string,public intent:string,public error:string){}
}


@Injectable()
export class ChatService {
  options : any;
  intent : string;
  parameter : string;
  readonly token = environment.dialogflow.angularBot;
  readonly client = new ApiAiClient({ accessToken: this.token});
  conversation = new BehaviorSubject<Message[]>([]);
  
  constructor(private http: HttpClient) { }

//Communication with dialogflow

converse(msg:string){
  if(msg == 'Initiate Jarvis'){
  const userMessage = new Message(msg,'user','','Default Welcome Intent','');
  this.update(userMessage);
}
else{
  const userMessage = new Message(msg,'user','','','');
  this.update(userMessage);
}


  return this.client.textRequest(msg)
  .then( res => {
    debugger;
    this.options = null;
    res.result.parameters.parameter ? this.parameter = res.result.parameters.parameter : this.parameter = "";
    // this.parameter = res.result.parameters.parameter;
    this.intent = res.result.metadata.intentName;
    switch(res.result.metadata.intentName)
    {
      case "ResellerSuccess":   if(res.result.parameters.parameter.includes("-"))
                          {
                            if(res.result.parameters.parameter.length > 9 || isNaN(res.result.parameters.parameter))
                              {
                             const speech = "Enter valid reseller id";
                             const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                             this.update(botMessage);
                               }
                            else{
                            const speech = res.result.fulfillment.speech;
                            const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'9char');
                            this.update(botMessage);
                            }
                          }
                          else{
                            if(res.result.parameters.parameter.length >8 || isNaN(res.result.parameters.parameter))
                              {
                             const speech = "Enter valid reseller id";
                             const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                             this.update(botMessage);
                               }
                            else{
                            const speech = res.result.fulfillment.speech;
                            const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'8char');
                            this.update(botMessage);
                            }
                          }
                        break;
        case "Reset-RMANumberIntent":
        case "RMANumberIntent": 
                            // if(res.result.parameters.parameter.includes("-"))
                            //             {
                            //                if(res.result.parameters.parameter.length > 10)
                            //              {
                            //                 const speech = "Enter valid RMA Number";
                            //                 const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                            //                 this.update(botMessage);
                            //               }
                            //               else{
                            //                 const speech = res.result.fulfillment.speech;
                            //                 this.parameter = res.result.parameters.parameter.slice(0, 2) + res.result.parameters.parameter.slice(3, res.result.parameters.parameter.length)
                            //                 const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'10char');
                            //                 this.update(botMessage);
                            //                }
                            //            }
                            //       else{
                            //                if(res.result.parameters.parameter.length > 9)
                            //                     {
                            //                      const speech = "Enter valid RMA Number";
                            //                      const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                            //                        this.update(botMessage);
                            //                      }
                            //                   else{
                            //                 const speech = res.result.fulfillment.speech;
                            //                      const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'9char');
                            //                        this.update(botMessage);
                            //                        }
                            //            }
                                       break;

        case "Reset-OrderNumberIntent":
        case "OrderNumberIntent": if(res.result.parameters.parameter.includes("-"))
                                        {
                                           if(res.result.parameters.parameter.length > 8)
                                         {
                                            const speech = "Enter valid order number";
                                            const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                                            this.update(botMessage);
                                          }
                                          else{
                                            const speech = res.result.fulfillment.speech;
                                            const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'8char');
                                            this.update(botMessage);
                                           }
                                       }
                                  else{
                                           if(res.result.parameters.parameter.length > 7)
                                                {
                                                 const speech = "Enter valid order number";
                                                 const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'error');
                                                   this.update(botMessage);
                                                 }
                                              else{
                                                 const speech = res.result.fulfillment.speech;
                                                 const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'7char');
                                                   this.update(botMessage);
                                                   }
                                       }
                                       break;
        default :  const speech = res.result.fulfillment.speech;
                        const botMessage = new Message(speech,'bot', this.parameter ,this.intent,'');
                        this.update(botMessage);
                        break;
      // case "PandA Service": this.http.get('http://localhost:41884/api/values/Get/'+res.result.parameters.SKU).subscribe(res=>{
      //                        this.options = res;
      //                          });
      //                       break;
    }

    

  })
}

pnaCall(sku,siteCode,custNumber)
  {
    // + btoa("APPPOWER:APPPOWER12345")
    //     const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Authorization': 'Basic QVBQUE9XRVI6QVBQUE9XRVIxMjM0NQ==',
    //     'content-type':"application/json"
    //   })
    // };
    
        var model =
    {  
       "servicerequest":{  
          "priceandstockrequest":{  
             
       
             "item":[  
                {  
                   "ingrampartnumber":sku
                   
                }
             ],
             "includeallsystems":false
          },
          "requestpreamble":{  
             "customernumber":custNumber,
             "isocountrycode":siteCode
          }
       }
    } ;

  //   var model = {  
  //     "servicerequest":{
  //          "requestpreamble":{  
  //           "customernumber":custNumber,
  //           "isocountrycode":siteCode
  //        },
  //        "priceandstockrequest":{  
  //           "showwarehouseavailability":"True",
  //           "extravailabilityflag":"Y",
  //           "item":[  
  //             {"ingrampartnumber":sku,"quantity":1},
  //  {"ingrampartnumber":sku,"quantity":1}
   
  //           ],
  //           "includeallsystems":false
  //        }
  //     }
  //  };
   
         return this.http.post('https://api.ingrammicro.com:443/multiskupriceandstockapi_v4', model);
        //  return this.http.post('https://api-beta.ingrammicro.com:443/multiskupriceandstockapi_v4_0_service', model);
        
    
      }

  //Add message to cource

  orderStatusCall(orderNumber,siteCode,custNumber)
  {
    // + btoa("APPPOWER:APPPOWER12345")
    
      //   var model =
      //   { 
      //     "servicerequest":{ 
      //          "requestpreamble":{ 
      //           "isocountrycode":"US", 
      //           "customernumber":custNumber 
      //        }, 
      //        "ordersearchrequest":{ 
      //            "systemid":"A300", 
      //           "searchcriteria":{ 
      //              "ordernumber":orderNumber 
      //           }, 
      //           "sortcriteria":[ 
                   
      //              { 
      //                 "orderby":"ordernumber", 
      //                 "orderbydirection":"DESC" 
      //              } 
      //           ], 
      //           "pagenumber":"1" 
      //        } 
             
      //     } 
      //  } ;
      var model = {"servicerequest":
                      {"requestpreamble":
                          {"isocountrycode":siteCode,
                           "customernumber":custNumber
                          },
                          "ordersearchrequest":
                          {"systemid":"A300",
                          "searchcriteria":{"ordernumber":orderNumber},
                          "sortcriteria"
                          :[{"orderby":"ordernumber","orderbydirection":"DESC"}],
                          "pagenumber":"1"}
                        }
                      };
         return this.http.post('https://api.ingrammicro.com:443/vse/orders/v2/ordersearch', model);
        //  return this.http.post(' https://api-beta.ingrammicro.com:443/vse/orders/v1/ordersearch', model);
        

    
      }

      rmaNumberCall(rmaNumber,siteCode,custNumber)
      {
            // var model =
            // {
            //   "servicerequest":{
            //     "requestpreamble":{
            //       "companycode":"MD",
            //       "customernumber":custNumber.substring(3,custNumber.length)
            //     //  "customernumber":"222222"

            //     },
            //     "rmareviewrecieptrequest":{
            //       "customerbranch":custNumber.substring(0,2),
            //       // "customernumber":"222222",
            //       "customernumber":custNumber.substring(3,custNumber.length),
            //       "rmabranch":rmaNumber.substring(0,2),
            //       "rmanumber":rmaNumber.substring(2,7),
            //       "distnumber":rmaNumber[7],
            //       "shipnumber":rmaNumber[8]
            //     }
            //   }
            // } ;
            var model = {
              "servicerequest":{
                "requestpreamble":{
                     "companycode":"MD",
                  "customernumber":custNumber.substring(3,custNumber.length)
                 },
                "rmareviewrecieptrequest":{
                  "customerbranch":custNumber.substring(0,2),
                        // "customernumber":"222222",
                        "customernumber":custNumber.substring(3,custNumber.length),
                        "rmabranch":rmaNumber.substring(0,2),
                        "rmanumber":rmaNumber.substring(2,7),
                        "distnumber":rmaNumber[7],
                        "shipnumber":rmaNumber[8]
                }
              }
            };
             return this.http.post('https://api.ingrammicro.com:443/api/rma/v1_1/rmareceipt', model);
            //  return this.http.post('https://api-beta.ingrammicro.com:443/api/rma/v1_1/rmareceipt', model);

             
        
          }
 

  update(msg:Message){
    this.conversation.next([msg]);

}



}

