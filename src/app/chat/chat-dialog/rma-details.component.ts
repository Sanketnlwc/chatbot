import { Component, OnInit,Input } from '@angular/core';
import { ChatService } from '../chat-service/chat.service';
@Component({
    selector: "rma-details",
  templateUrl: './rma-details.component.html',
})
export class RMADetailsComponent implements OnInit {
    @Input() public rmaNumber: string;
    @Input() public siteCode:string;
    @Input() public customer:string;
    rmaStatus:string;
    rmaNumberRes:string;
    totalLineItems:string;
    showLineItems:boolean = false;
    lineItems :{};
    isFailure:boolean = true;
    failureMessage:string;
    loading:boolean = true;
    errorMessage:string;
    

  slideConfig = {"slidesToShow": 2, "slidesToScroll": 1,"infinite":false,"variableWidth":true,
      "nextArrow": ' <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>',
      "prevArrow": ' <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'};
     constructor(public chat:ChatService) {}
     ngOnInit() {
          if(this.rmaNumber.includes("-"))
                                        {
                                           if(this.rmaNumber.length > 10)
                                         {
                                            this.errorMessage = "Enter valid RMA Number";
                                          }
                                          else{
                                            this.rmaNumber =this.rmaNumber.slice(0, 2) + this.rmaNumber.slice(3, this.rmaNumber.length)
                                           
                                            rmaCall();
                                          }
                                       }
                                  else{
                                           if(this.rmaNumber.length > 9){
                                             
                                            this.errorMessage = "Enter valid RMA Number";
                                           }
                                          else{
                                            rmaCall();
                                          }
                                       }
          
    
    function rmaCall(){
       this.chat.rmaNumberCall(this.rmaNumber,this.siteCode,this.customer).subscribe((res : any)=> {
                                       this.isFailure = false;
                                       this.loading = false;
                                                            console.log(res);
                                if(res.serviceresponse.responsepreamble.requeststatus == 'SUCCESS'){
                                                   this.rmaNumberRes = res.serviceresponse.rmareceiptwebresponse.rmanumber;
                                                   this.rmaStatus = res.serviceresponse.rmareceiptwebresponse.rmastatus;
                                                    if(res.serviceresponse.rmareceiptwebresponse.lineinfo)
                                                           {
                                    
                                                         this.lineItems = res.serviceresponse.rmareceiptwebresponse.lineinfo;
                                                         console.log(this.lineItems);
                                                           this.totalLineItems = res.serviceresponse.rmareceiptwebresponse.lineinfo.length;
                                                               }
                                                    }
                                  else{
                                    this.isFailure = true;
                                    this.failureMessage = res.serviceresponse.responsepreamble.returnmessage;
                                  } 
           });
    }
    }

}

