import { Component, OnInit } from '@angular/core';
import { ChatService, Message } from '../chat-service/chat.service';
import { Observable } from 'rxjs/Observable';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/scan';
import 'rxjs/add/observable/empty' 
@Component({
  selector: 'app-chat-dialog',
  templateUrl: './chat-dialog.component.html',
  styleUrls: ['./chat-dialog.component.css']
})

export class ChatDialogComponent implements OnInit {
  welcomeIntent : boolean;
  messages: Observable<Message[]>;
  formValue:string;
  username:string;
  countryInput : boolean;
  intentName : string;
  options : any[];
  countryName: string;
  siteCode: string;
  sku : string;
  description : string;
  vpn : string;
  price : string;
  stock : string;
  errorMessage : string;
  customerNumber : string;
  orderDetails : boolean;
  rmaDetails : boolean;
  
  orderNumber : string;
  orderDate : string;
  orderStatus : string;

  rmaNumber : string;
  rmaStatus : string;
afterReset:boolean;
  isReset : boolean;
  resetOptions : any[];

  totalLineItems : string;
  showLineItems:boolean = false;
  totalInstock : number = 0;
  lineItems :{};

  product:any;
  prod:boolean;

  error:boolean;

  statesComplex: any[] = [
    {name: 'Australia', siteCode: 'AU' },
    {name: 'United States', siteCode: 'US' },
    {name: 'Spain', siteCode: 'ES' },
    {name: 'Mexico', siteCode: 'MX' }
  ];

  constructor(public chat:ChatService, private route:ActivatedRoute) {
   }

  ngOnInit() {

    this.route.params.subscribe(params => {
      this.username = params['username'];
      this.chat.converse('Initiate Jarvis');
    });

    if(window.localStorage.getItem('isReset')){
      this.isReset = true;
      this.resetOptions = ["Continue","Reset Reseller And Country"];
      this.siteCode = window.localStorage.getItem("siteCode");
      this.customerNumber = window.localStorage.getItem("customerNumber");
      this.countryName = window.localStorage.getItem("countryName");
      window.localStorage.clear();
      this.options = [];
      if(this.countryInput)
        this.countryInput = false;
 }
else{
  this.welcomeIntent = true;
}
//appending the messages incoming

this.messages = this.chat.conversation.asObservable()
  .scan(
    (acc,val)=>
     acc.concat(val)
    );



  this.messages.subscribe(res=> {
    console.log(res);
    this.prod = false;
    debugger;
    if(res.length > 0)
    {
      this.options = [];
      var length = res.length - 1;
      switch(res[length].intent)
      {
         case "Default Welcome Intent": this.countryInput = true;
                                        break;
        case "ResellerSuccess": if(res[length].parameter.includes("-"))
                          {
                            if(res[length].parameter.length > 9 )
                              {
                             this.errorMessage = "Enter valid reseller id";
                               }
                            else{
                              this.customerNumber = res[res.length-1].parameter;
                              this.options = ["RMA Status","Order Status","Price And Availability"];
                            }
                          }
                          else{
                            if(res[length].parameter.length >8 )
                              {
                                this.errorMessage = "Enter valid reseller id";
                               }
                            else{
                              this.customerNumber = res[res.length-1].parameter;
                              this.options = ["RMA Status","Order Status","Price And Availability"];
                            }
                          }
                        break;
                          //     if(res[length].error == '9char')
                          //   {
                          //     this.customerNumber = res[res.length-1].parameter;
                          //     this.options = ["RMA Status","Order Status","Price And Availability"];
                          //   }
                          // else if(res[length].error == '8char')
                          //     {
                          //     this.customerNumber = res[res.length-1].parameter.substring(0, 2) + '-' + res[res.length-1].parameter.substring(2, res[res.length-1].parameter.length);
                          //     this.options = ["RMA Status","Order Status","Price And Availability"];
                          //     }
        case "Reset-EnterSkuIntent":
        case "EnterSkuIntent": 
                                  this.sku = res[res.length-1].parameter.toUpperCase();
                                  this.options = ["RMA Status","Order Status","Price And Availability"];
                                  break;
        case "Reset-OrderNumberIntent":
        case "OrderNumberIntent":
                                    if(res[length].error == '8char')
                                          {
                                             this.orderDetails = true;
                                             this.orderNumber = res[length].parameter;
                                             this.options = ["RMA Status","Order Status","Price And Availability"];
                                            
                                           }
                                else if(res[length].error == '7char'){        
                                              this.orderDetails = true;
                                              this.orderNumber = res[length].parameter.substring(0, 2) + '-' + res[length].parameter.substring(2, res[length].parameter.length);
                                              this.options = ["RMA Status","Order Status","Price And Availability"];
                                              // this.chat.orderStatusCall(this.orderNumber,this.siteCode,this.customerNumber).subscribe((res : any)=> {
                                            //    console.log(res);
                                            //   this.orderDate = res.serviceresponse.ordersearchresponse.ordersummary["0"].entrytimestamp;
                                            //   this.orderStatus = res.serviceresponse.ordersearchresponse.ordersummary["0"].orderstatus;
                                            //  });
                                     }
                                  break;
        case "Reset-RMANumberIntent":   
        case "RMANumberIntent":   
                                    if(res[length].error == '10char')
                                          {
                                             this.rmaDetails = true;
                                             this.options = ["RMA Status","Order Status","Price And Availability"];
                                             this.rmaNumber = res[length].parameter;
                                           }
                                else if(res[length].error == '9char'){        
                                              this.rmaDetails = true;
                                              this.options = ["RMA Status","Order Status","Price And Availability"];
                                              this.rmaNumber = res[res.length-1].parameter;
                                              // this.chat.rmaNumberCall(res[res.length-1].parameter,this.siteCode,this.customerNumber).subscribe((res : any)=> {
                                              //               console.log(res);
                                              //      this.rmaNumber = res.serviceresponse.rmareceiptwebresponse.rmanumber;
                                              //      this.rmaStatus = res.serviceresponse.rmareceiptwebresponse.rmastatus;
                                              //       if(res.serviceresponse.rmareceiptwebresponse.lineinfo)
                                              //              {
                                              //            this.lineItems = res.serviceresponse.rmareceiptwebresponse.lineinfo;
                                              //              this.totalLineItems = res.serviceresponse.rmareceiptwebresponse.lineinfo.length;
                                              //                  }
                                              //       });
                                     }
                                  break;
        case "Reset Continue":
                                  this.options = ["RMA Status","Order Status","Price And Availability"];
                                  this.countryInput = false;
                                  break;
        case "ResetResellerCountry":
                                  this.afterReset = true;
                                  this.countryInput = true;
                                  break;
      }
    }
    })

  }

reset(){
  window.localStorage.setItem('isReset',"yes");
  window.localStorage.setItem('siteCode',this.siteCode);
  window.localStorage.setItem('countryName',this.countryName);
  window.localStorage.setItem('customerNumber',this.customerNumber);
  location.reload();
}

  sendMessage(country){
    debugger;
    if(country){
    this.countryName = this.formValue;
      this.countryInput = false;
      this.statesComplex.map(element => {
        if(this.formValue == element.name)
          {
            this.siteCode = element.siteCode;
              }
      })
    }
    this.chat.converse(this.formValue);
    this.formValue = '';
  }

   optionSend(option)
  {
    this.formValue = option;
    this.sendMessage('');
  }

  optionResetSend(option)
  {
    if(option == "Reset Reseller And Country")
      location.reload();
    else 
    {
      this.isReset = false;
      this.formValue = option;
    this.sendMessage('');
    }
  }

}
