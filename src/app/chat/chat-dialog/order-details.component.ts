import { Component, OnInit,Input } from '@angular/core';
import { ChatService } from '../chat-service/chat.service';
@Component({
    selector: "order-details",
  templateUrl: './order-details.component.html',
})
export class OrderDetailsComponent implements OnInit {
    @Input() public orderNumber: string;
    @Input() public siteCode:string;
    @Input() public customer:string;
    orderDate:string;
    orderStatus:string;
    orderNumberRes: string;
    isFailure:boolean = true;
    failureMessage:string;
    loading:boolean = true;
    totalLineItems:string;
    showLineItems:boolean = false;
    lineItems :{};
     slideConfig = {"slidesToShow": 2, "slidesToScroll": 1,"infinite":false,"variableWidth":true,
      "nextArrow": ' <i class="fa fa-chevron-circle-right" aria-hidden="true"></i>',
      "prevArrow": ' <i class="fa fa-chevron-circle-left" aria-hidden="true"></i>'};
     constructor(public chat:ChatService) {}
     ngOnInit() {
         debugger;
          this.chat.orderStatusCall(this.orderNumber,this.siteCode,this.customer).subscribe((res : any)=> {
         this.isFailure = false;
         this.loading = false;
                                console.log(res);
                                if(res.serviceresponse.ordersearchresponse.numberoforderresults != "0"){
                                             console.log(res);
                                             this.orderNumberRes = res.serviceresponse.ordersearchresponse.ordersummary["0"].ordernumber;
                                             this.orderDate = res.serviceresponse.ordersearchresponse.ordersummary["0"].entrytimestamp;
                                             this.orderStatus = res.serviceresponse.ordersearchresponse.ordersummary["0"].orderstatus;
                                              if(res.serviceresponse.ordersearchresponse.ordersummary["0"].lines)
                                                           {
                                                          this.lineItems = res.serviceresponse.ordersearchresponse.ordersummary["0"].lines;
                                                         console.log(this.lineItems);
                                                           this.totalLineItems = res.serviceresponse.ordersearchresponse.ordersummary["0"].lines.length;
                                                               }
                                }
                                else{
                                    this.isFailure = true;
                                    this.failureMessage = "No Data Found Enter Another Order Number";
                                }
             });
    
    }
}

