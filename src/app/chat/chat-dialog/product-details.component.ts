import { Component, Directive, OnInit, ElementRef, Input } from '@angular/core';
import { ChatService, Message} from '../chat-service/chat.service';
@Component({
    selector: "product-details",
  templateUrl: './product-details.component.html',
})
export class ProductLineDetailsComponent implements OnInit {
    @Input() public product: string;
    @Input() public siteCode:string;
    @Input() public customer:string;
    sku:string;
    vpn:string;
    description:string;
    price:string;
    totalInstock:number=0;
    isFailure:boolean = true;
    failureMessage:string;
    loading:boolean = true;
     constructor(public chat:ChatService) {}
     ngOnInit() {   
         this.chat.pnaCall(this.product,this.siteCode,this.customer).toPromise().then((res1 : any)=> {
         this.isFailure = false;
         this.loading = false;
                                console.log(res1);
                                if(res1.serviceresponse.responsepreamble.responsestatus == 'SUCCESS'){
                                  this.sku = res1.serviceresponse.priceandstockresponse.details["0"].ingrampartnumber;
                                  if(res1.serviceresponse.priceandstockresponse.details["0"].partdescription2)
                                    this.description = res1.serviceresponse.priceandstockresponse.details["0"].partdescription1 + res1.serviceresponse.priceandstockresponse.details["0"].partdescription2;
                                  else
                                     this.description = res1.serviceresponse.priceandstockresponse.details["0"].partdescription1;
                                  this.vpn = res1.serviceresponse.priceandstockresponse.details["0"].vendorpartnumber;
                                  this.price = res1.serviceresponse.priceandstockresponse.details["0"].currency + " "+res1.serviceresponse.priceandstockresponse.details["0"].retailprice;
                                  res1.serviceresponse.priceandstockresponse.details["0"].warehousedetails.map(element=>{
                                      this.totalInstock = this.totalInstock + element.availablequantity;
                                  });
                                }
                                else{
                                    this.isFailure = true;
                                    this.failureMessage = res1.serviceresponse.responsepreamble.responsemessage;
                                }
             });
    
    }
}

