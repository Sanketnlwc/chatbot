import { Routes, RouterModule } from "@angular/router";

import { AppComponent } from './app.component';
 import { ChatDialogComponent } from './chat/chat-dialog/chat-dialog.component'
 import { LoginComponent } from './login/login.component'
 import { IframeComponent } from './iframe/iframe.component'
//import { RootComponent }  from './root.component';


const routes: Routes = [
    {
        path:'' , redirectTo: 'login', pathMatch: 'full' 
    },
    {
        path: 'login', component: LoginComponent
    },
     {
        path: 'chat/:username', component: ChatDialogComponent  }
];

export const routingInfo = RouterModule.forRoot(routes);

