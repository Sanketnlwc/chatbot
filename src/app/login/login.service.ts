import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class LoginService {

  constructor(private http: HttpClient) {

   }


  getlogindata(username,password){

    
    var model = {
    "request":{
      "requestpreamble":null,
        "servicerequest":{
         
          "password":password,
           "username":username
        }
        
      }
  };
     return this.http.post('https://api.ingrammicro.com:443/api/user/v1/authenticateuser', model);


  }

}
