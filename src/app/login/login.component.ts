import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Router } from '@angular/router';
import {LoginResponse } from './login.interface';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers : [LoginService]
})
export class LoginComponent implements OnInit {

  usernameValue:string;
  passwordValue:string;
  error:boolean = false;
  loading:boolean = false;
  constructor( private _loginService:LoginService, private router:Router) { }

  ngOnInit() {
  }

  login(){
    this.loading = true;
    this._loginService.getlogindata(this.usernameValue,this.passwordValue).subscribe((res : any)=> {
    this.loading = false;
      // let result = res.json();
      debugger;
      if(res.response.responsepreamble.returnstatus == "Success")
      {

           this.router.navigate(['../chat',res.response.serviceresponse.username]);
      }
      else{
        this.error = true;
      }
    });
  }

}
