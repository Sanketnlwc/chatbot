export interface LoginResponse{
    response : Response[];
}

 export interface Response {
    responsepreamble : ResponsePreamble[];
    serviceresponse : ServiceResponse[];
}

export interface ResponsePreamble{
    returnstatus: string;
    returnmessages : ReturnMessages[];
}

export interface ServiceResponse{
    isauthenticated: boolean;
      status: string;
      username: string;
      emailID: string;
      userID: string;
      displayname: string;
}

interface ReturnMessages extends ResponsePreamble{
    information : string[];
}